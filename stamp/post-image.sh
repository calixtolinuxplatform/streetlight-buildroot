#!/bin/bash

BOARD_DIR="board/streetlight/stamp"
TEMP_DIR=$BOARD_DIR/"temp"
IMAGE_DIR=$BOARD_DIR/"images/nand/"
IMAGE_DIR2=$BOARD_DIR/"images/mmc/"
OUT_DIR=$IMAGE_DIR/
OUT_DIR2=$IMAGE_DIR2/

CREATE_IMAGE="create-flasher-image.sh"
DEBRICK_SCR="debrick.scr"
OUT_FILE="calixto-stamp.out"



mkdir -p $TEMP_DIR

cp output/images/MLO.byteswap $TEMP_DIR
cp output/images/u-boot.img $TEMP_DIR
cp output/images/am335x-calixto-stamp.dtb $TEMP_DIR
cp output/images/zImage $TEMP_DIR
cp output/images/rootfs.ubi $TEMP_DIR

cp $BOARD_DIR/$CREATE_IMAGE $TEMP_DIR

cd $TEMP_DIR

./$CREATE_IMAGE

cd ../../../../

mkdir -p $OUT_DIR

cp $TEMP_DIR/$DEBRICK_SCR $OUT_DIR
cp $TEMP_DIR/$OUT_FILE $OUT_DIR

cp $TEMP_DIR/$DEBRICK_SCR $IMAGE_DIR
cp $TEMP_DIR/$OUT_FILE $IMAGE_DIR

sync


rm -rf $TEMP_DIR

mkdir -p $OUT_DIR2
cp output/images/MLO $OUT_DIR2
cp output/images/u-boot.img $OUT_DIR2
cp output/images/rootfs.tar $OUT_DIR2






