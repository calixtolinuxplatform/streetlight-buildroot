#!/bin/sh

BOARD_DIR="board/streetlight/stamp/"

install -d -m 755 $TARGET_DIR/lib/firmware/
install -D -m 755 $BOARD_DIR/amx3-cm3/am335x-pm-firmware.elf 	$TARGET_DIR/lib/firmware/

install -D -m 755 $BOARD_DIR/u-boot-fw-utils_2013.10/fw_printenv	$TARGET_DIR/sbin/
install -D -m 755 $BOARD_DIR/u-boot-fw-utils_2013.10/fw_setenv 		$TARGET_DIR/sbin/
install -D -m 644 $BOARD_DIR/u-boot-fw-utils_2013.10/fw_env.config 	$TARGET_DIR/etc/

install -D -m 755 $BOARD_DIR/wvdial/wvdial.conf 		$TARGET_DIR/etc/
install -D -m 644 $BOARD_DIR/wvdial/libwvbase.so.4.6 		$TARGET_DIR/usr/lib/



