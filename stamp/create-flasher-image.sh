#!/bin/bash

IMAGE_FILES="MLO.byteswap u-boot.img am335x-calixto-stamp.dtb zImage rootfs.ubi"

BLOCK_SIZE=2048

OUTFILE="calixto-stamp.out"
DEBRICKFILE="debrick.txt"
DEBRICKIMAGE="debrick.scr"
offset=0x82000000


for f in $IMAGE_FILES
do 
  cat $f
  head -c $((($BLOCK_SIZE-$(stat -c %s $f)%$BLOCK_SIZE)%$BLOCK_SIZE)) /dev/zero 
done >$OUTFILE

i=0
for f in $IMAGE_FILES
do
  IMAGE_FILES[$i]=$f
  IMAGE_LENGTHS[$i]=$(($(stat -c %s $f)))
  IMAGE_LENGTHS[$i]=$(printf "0x%x" ${IMAGE_LENGTHS[$i]})
  IMAGE_OFFSETS[$i]=$(($offset))
  IMAGE_OFFSETS[$i]=$(printf "0x%x" ${IMAGE_OFFSETS[$i]})
  offset=$(($offset+${IMAGE_LENGTHS[$i]}+$BLOCK_SIZE-$(stat -c %s $f)%$BLOCK_SIZE))
  i=$i+1
done
  
if [ ! -e "$DEBRICKFILE" ] ; then
    touch "$DEBRICKFILE"
fi



cat > $DEBRICKFILE << EOF1
#
# Module: bootflasher script
#
# Description: This program is used to demostrate concatenate binary
#              images.
#
# Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
#
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#    Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#

# expect the following parameters to be set outside this script
# serverip which should happen as part of the dhcp process
# 
# Modified by : Sooraj <sooraj.rk@calixto.co.in>
# Modified from : debrick.txt


setenv READY_RETRY_DELAY 5
setenv IMAGE_CNT 5

# Name of Image containing all images to be tftp'd from host
setenv ImageName ${OUTFILE}

#place values from flash-cat-utility here
setenv Image1_Name ${IMAGE_FILES[0]}
setenv Image1_DDR_ADDR ${IMAGE_OFFSETS[0]}
setenv Image1_Length ${IMAGE_LENGTHS[0]}
setenv Image1_NOR_Offset  0x0

setenv Image2_Name ${IMAGE_FILES[1]}
setenv Image2_DDR_ADDR ${IMAGE_OFFSETS[1]}
setenv Image2_Length ${IMAGE_LENGTHS[1]}
setenv Image2_NOR_Offset  0x20000

setenv Image3_Name ${IMAGE_FILES[2]}
setenv Image3_DDR_ADDR ${IMAGE_OFFSETS[2]}
setenv Image3_Length ${IMAGE_LENGTHS[2]}
setenv Image3_NAND_Offset 0x00000000

setenv Image4_Name ${IMAGE_FILES[3]}
setenv Image4_DDR_ADDR ${IMAGE_OFFSETS[3]}
setenv Image4_Length ${IMAGE_LENGTHS[3]}
setenv Image4_NAND_Offset 0x00020000

setenv Image5_Name ${IMAGE_FILES[4]}
setenv Image5_DDR_ADDR ${IMAGE_OFFSETS[4]}
setenv Image5_Length ${IMAGE_LENGTHS[4]}
setenv Image5_NAND_Offset 0x01020000

setenv nor_storage_offset 0x000000
setenv nor_erase_length 0x0E0000
	
# Set File Names to indicate progress <DO NOT MODIFY>
setenv HostReadyFile HostReadyFile:\${ethaddr}
setenv DebrickTargetReady DebrickTargetReady:\${ethaddr}
setenv TargetProgrammingComplete TargetProgrammingComplete:\${ethaddr}
setenv TargetErrorExit TargetErrorExit:\${ethaddr}
setenv TargetEraseError TargetEraseError:\${ethaddr}
setenv TargetProgrammingError TargetProgrammingError:\${ethaddr}
setenv TargetGetFileError TargetGetFileError:\${ethaddr}

#Need to halt processor due to a processing error in the script
setenv DataAbort 'echo Halting processor.....; go 0x2001c;'

# Define addresses where the messages will be sourced for the TFTP puts
setenv image_msg1_addr 0x80008100

# Define System status messages of flashing  <DO NOT MODIFY> 
setenv GetReadyFileFromHost "tftp 0x80001000 \${serverip}:\${HostReadyFile}"
setenv DebrickStartingToHost "tftp 0x80001000 \${serverip}:\${DebrickTargetReady}"
setenv TargetProgrammingComplete "tftp 0x80000F00 \${serverip}:\${TargetProgrammingComplete}"
setenv TargetProgrammingFailure "tftp 0x80001000 \${serverip}:\${TargetProgrammingError}"
setenv TargetErrorExit "tftp 0x80001000 \${serverip}:\${TargetErrorExit}"
setenv TargetEraseErrorExit "tftp 0x80001000 \${serverip}:\${TargetEraseError}"
setenv TargetGetFileFailure "tftp 0x80001000 \${serverip}:\${TargetGetFileError}"

# Define Image succesful Complete get commands, these requests are detected
# the gets will fail as the files don't exist, that ok. Using this as a 
# feedback feedback mechanism to indicate statusi
# <DO NOT MODIFY>
setenv DoneWithImage "tftp \${image_msg1_addr} \${serverip}:\${ImageName}_Complete"


# Define Image get command  <DO NOT MODIFY>
setenv GetImage 'tftpboot \${loadaddr} \${serverip}:\${ImageName}'

# Erase NAND Macro  <Does not need to be Modified, typically>
# 
setenv EraseNand 'echo Erasing NAND; if nand erase.chip; then echo NAND Erased; echo; else echo NAND Erase Failure Error Exit;  gpio clear 46; gpio set 27;run DataAbort; fi;'

setenv ProbeSFlash 'echo Probing Serial Flash..; if sf probe 0; then echo Serial flash detected; echo; else  gpio clear 46; gpio set 27;run DataAbort; fi;'

setenv EraseNor 'echo Erasing Serial Flash \${nor_storage_offset} - \${nor_erase_length}..; if sf erase \${nor_storage_offset} \${nor_erase_length}; then echo SFlash Erased; echo; else echo SFlash Erase Failure Error Exit;   gpio clear 46;gpio set 27; run DataAbort; fi;'

# Get Image Macro  <Does not need to be Modified, typically>
setenv GetFlashImage 'if run GetImage; then echo; echo Got Flash Image; else echo Get Flash Image Failed;   gpio clear 46;gpio set 27;run DataAbort; fi;'

# Flash Image Macro <Does not need to be Modified, typically>
setenv FlashImage 'if nand write \${source_addr} \${storage_offset} \${image_length}; then echo; echo   \${ImageName} succesfully programmed;echo; else echo \${ImageName} Programming Failed... error exit;   gpio clear 46;gpio set 27; run DataAbort; fi;'

# Flash Image NOR Macro <Does not need to be Modified, typically>
setenv FlashImageNor 'if sf write \${source_addr} \${storage_offset} \${image_length}; then echo; echo   \${ImageName} succesfully programmed;echo; else echo \${ImageName} Programming Failed... error exit;   gpio clear 46;gpio set 27; run DataAbort; fi;'


# -------------------------------------------------------------------
# --- Execution Starts Here
# -------------------------------------------------------------------
# --- Start The Process of Flashing
#
echo 
echo "The Debrick script is running"
echo 

# -------------------------------------------------------------------
# On Blue led
# -------------------------------------------------------------------
gpio set 109

#
# <<<<<<<<<<<< Start of Image Flashing>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#
# -------------------------------------------------------------------
# Get the single image containing all the files.
# -------------------------------------------------------------------
echo
echo Get Image containing all images to be flashed
echo
run GetFlashImage


# -------------------------------------------------------------------
# On Yellow led
# -------------------------------------------------------------------
gpio set 113


run ProbeSFlash
run EraseNor

# -------------------------------------------------------------------
# Add a section for each Image to be flashed
# For each Image to be flashed you will need to set the 
# following environments:
#  - Source Address in the Single Image
#  - Storage Offset into the Storage device
#  - Length of the image 
#  - Image Name
# -------------------------------------------------------------------

# Flash Image 1
#--------------------------------------------------------------------
setenv source_addr \${Image1_DDR_ADDR}
setenv storage_offset \${Image1_NOR_Offset}
setenv image_length \${Image1_Length}
setenv ImageName \${Image1_Name}
run FlashImageNor
#--------------------------------------------------------------------


# Flash Image 2
#--------------------------------------------------------------------
setenv source_addr \${Image2_DDR_ADDR}
setenv storage_offset \${Image2_NOR_Offset}
setenv image_length \${Image2_Length}
setenv ImageName \${Image2_Name}
run FlashImageNor
#--------------------------------------------------------------------


# -------------------------------------------------------------------
# On Red led
# -------------------------------------------------------------------
gpio set 117

run EraseNand

# Flash Image 3
#--------------------------------------------------------------------
setenv source_addr \${Image3_DDR_ADDR}
setenv storage_offset \${Image3_NAND_Offset}
setenv image_length \${Image3_Length}
setenv ImageName \${Image3_Name}
run FlashImage
#--------------------------------------------------------------------

# Flash Image 4
#--------------------------------------------------------------------
setenv source_addr \${Image4_DDR_ADDR}
setenv storage_offset \${Image4_NAND_Offset}
setenv image_length \${Image4_Length}
setenv ImageName \${Image4_Name}
run FlashImage
#--------------------------------------------------------------------

# Flash Image 5
#--------------------------------------------------------------------
setenv source_addr \${Image5_DDR_ADDR}
setenv storage_offset \${Image5_NAND_Offset}
setenv image_length \${Image5_Length}
setenv ImageName \${Image5_Name}
run FlashImage
#--------------------------------------------------------------------

# -------------------------------------------------------------------
# On Red led
# -------------------------------------------------------------------
gpio set 116

#
# <<<<<<<<<<<<<  End of Image Flashing  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#
#
# -------------------------------------------------------------------
# Indicate to Host successful completion
run TargetProgrammingComplete
echo
echo " Target Flash Complete........"
echo

EOF1

mkimage -A arm -O U-Boot -C none -T script -d $DEBRICKFILE $DEBRICKIMAGE





